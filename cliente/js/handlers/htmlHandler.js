function updateLivesHtml() {
	var container = document.getElementById('lives');
	if (!container) {
		container = createLivesHtml();
		if (!container) {
			return;
		}
	}
	container.innerHTML = '';
	if (player && player.lives && player.lives > 0) {
		for (var i = 0; i < player.lives; i++) {
			var newDiv = document.createElement('div');
			newDiv.className = 'heart';
			newDiv.style.left = (16 * i) + 'px';
			container.appendChild(newDiv);
		}
	} else {
		var newDiv = document.createElement('div');
		newDiv.style.width = '100%';
		newDiv.style.height = '100%';
		newDiv.style.background = 'rgba(255, 0, 0, 0.4)';
		newDiv.style.position = 'absolute';
		newDiv.style.lineHeight = window.innerHeight + 'px';
		newDiv.style.textAlign = 'center';
		newDiv.style.color = 'white';
		newDiv.style.fontSize = 20 + 'px';
		newDiv.style.textShadow = '2px 0 0 #000, -2px 0 0 #000, 0 2px 0 #000, 0 -2px 0 #000, 1px 1px #000, -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000';
		newDiv.style.zIndex = 1;
		newDiv.onclick = restart;
		newDiv.innerHTML = 'You died, click the game to restart';
		container.appendChild(newDiv);
	}
}

function createLivesHtml() {
	var canvas = document.getElementById('overlay');
	if (canvas) {
		var livesDiv = document.createElement('div');
		livesDiv.id = 'lives';
		canvas.appendChild(livesDiv);
		return livesDiv;
	}
}

function updateKillsHtml() {
	var element = document.getElementById('kills');
	if (!element) {
		element = createKillsHtml();
		if (!element) {
			return;
		}
	}
	element.innerHTML = '';
	var newDiv = document.createElement('div');
	newDiv.style.right = '0px';
	newDiv.style.padding = '5px';
	newDiv.style.position = 'absolute';
	newDiv.style.color = 'white';
	newDiv.style.textShadow = '1px 0 0 #000, -1px 0 0 #000, 0 1px 0 #000, 0 -1px 0 #000, 1px 1px #000, -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000';
	newDiv.innerHTML = 'Kills: ' + player.kill;
	element.appendChild(newDiv);
}

function createKillsHtml() {
	var canvas = document.getElementById('overlay');
	if (canvas) {
		var killsDiv = document.createElement('div');
		killsDiv.id = 'kills';
		canvas.appendChild(killsDiv);
		return killsDiv;
	}
}

function createHPHtml() {
	var canvas = document.getElementById('overlay');
	if (canvas) {
		var hpDiv = document.createElement('div');
		hpDiv.id = 'hp';
		canvas.appendChild(hpDiv);
		hpDiv.style.width = 'calc(100% - 2px)';
		hpDiv.style.height = '5%';
		hpDiv.style.position = 'absolute';
		hpDiv.style.left = '0px';
		hpDiv.style.bottom = '0px';
		hpDiv.style.background = 'red';
		hpDiv.style.border = '1px solid';
		return hpDiv;
	}
}

function updateHPHtml() {
	var element = document.getElementById('hp');
	if (!element) {
		element = createHPHtml();
		if (!element) {
			return;
		}
	}
	element.innerHTML = '';
	var hpMax = 100;
	var newDiv = document.createElement('div');
	newDiv.style.background = 'green';
	newDiv.style.width = ((hpMax / 10) * player.hp) + '%';
	newDiv.style.height = '100%';
	element.appendChild(newDiv);
}

function createInfoHtml() {
	var element = document.getElementById('info');
	element.style.position = 'absolute';
	element.style.right = (WIDTH_SCREEN_HALF + WIDTH_HALF) + 'px';
	element.style.top = (GAP_TOP_SCREEN) + 'px';
	element.style.padding = 5 + 'px';
	element.innerHTML = 'Use WASD para se mover</br>Use o botão esquerdo do mouse para atacar</br>Use ENTER para pausar</br>Use < para diminuir o volume</br>Use > para aumentar o volume</br>Use M para mutar ou desmutar';
}

function createVolumeMenu() {
	var element = document.getElementById('volumeMenu');
	element.style.position = 'absolute';
	element.style.left = (WIDTH_SCREEN_HALF + WIDTH_HALF) + 'px';
	element.style.top = (GAP_TOP_SCREEN) + 'px';
	element.style.padding = 5 + 'px';
	element.innerHTML = 'Volume</br><input type="range" id="myRange" value="50" onChange="changeVolume();">';
}

function changeVolume() {
	if (document.getElementById("myRange")) {
		volume = document.getElementById("myRange").value / 100;
		console.log(volume);
		if (player) {
			for (var i in player.children) {
				if (player.children[i].type === 'Audio') {
					player.children[i].setVolume(volume);
				}
			}
		}
	}
}

function createScoreboardHtml(scoreList) {
	var newDiv = document.createElement('div');
	newDiv.id = 'scoreboard';
	newDiv.innerHTML = '<div id="closeScoreboard" onClick="closeScoreboardScreen();" class="menu-button"><span>Back<span></div>' +
	'<div id="scoreboardList"></div>';
	document.getElementById('menus').appendChild(newDiv);
	return newDiv;
}

function updateScoreboardHtml(scoreList) {
	var scoreboard = getScoreboardElement();
	if (!scoreboard) {
		scoreboard = createScoreboardHtml(scoreList);
	}
	//scoreboard.innerHTML = '';
	var scoreboardList = document.getElementById('scoreboardList');
	for(i in scoreList) {
		if (scoreList[i].kill !== undefined) {
			var newScore = document.createElement('div');
			if (!scoreList[i].name) {
				scoreList[i].name = 'Guest';
			}
			newScore.innerHTML = scoreList[i].name + ' - Kills: ' + scoreList[i].kill;
			scoreboardList.appendChild(newScore);
		}
	}
}

function createOptionsHtml() {
	var newDiv = document.createElement('div');
	newDiv.id = 'options';
	newDiv.innerHTML = '<div id="closeOptions" onClick="closeOptionsScreen();" class="menu-button"><span>Back<span></div>' +
		'<div><div>Volume</div><div><input type="range" id="myRange" value="' + (volume * 100) + '" onChange="changeVolume();"></div></div>' +
		'<div id="optionsCommandContent">' +
		'<div>WASD keys to move</div>' +
		'<div>Left mouse button to attack</div>' +
		'<div>ENTER to pause the game</div>' +
		'<div>Use < to down volume</div>' +
		'<div>Use > to rise volume</div>' +
		'<div>Use M to mute or unmute</div>' +
		'</div>';
	document.getElementById('menus').appendChild(newDiv);
}

function closeOptionsScreen() {
	var element = document.getElementById('options');
	element.parentNode.removeChild(element);
}

function closeScoreboardScreen() {
	var element = getScoreboardElement();
	element.parentNode.removeChild(element);
}

function getScoreboardElement() {
	var scoreboard = document.getElementById('scoreboard');
	if (scoreboard) {
		return scoreboard;
	} else {
		return null;
	}
}

function createLoginMenu() {
	var newDiv = document.createElement('div');
	newDiv.id = 'loginMenu';
	newDiv.innerHTML = '<div id="loginPanel"><div id="inputsLoginPanel">Name <input id="player-name"><br/>Password <input id="player-password" type="password"></div><br/><button onclick="login()">Register/Login</button><br/><div style="padding-top: 10px;"> <button onClick="goToScoreBoard();">ScoreBoard</button></div></div>';
	document.getElementById('menus').appendChild(newDiv);
}

function toggleMenuLogin() {
	var loginMenu = document.getElementById('login-menu');
	if (loginMenu.style.visibility === 'visible') {
		loginMenu.style.visibility = 'hidden';
	} else {
		loginMenu.style.visibility = 'visible';
	}
}

function getMenuLoginValues() {
	var account = {
		name: document.getElementById('player-name').value,
		password: document.getElementById('player-password').value
	}
	return account;
}

function removeLoginMenu(callback) {
	var loginMenu = document.getElementById("loginMenu");
	if (loginMenu.parentNode) {
	  loginMenu.parentNode.removeChild(loginMenu);
	}
	if (callback) {
		callback();
	}
}

function createMainMenu() {
	var menusDiv = document.getElementById('menus');
	if (menusDiv) {
		var mainMenuDiv = document.createElement('div');
		mainMenuDiv.id = 'mainMenu';
		mainMenuDiv.innerHTML = '<div id="startGame" onClick="init();" class="menu-button"><span>Start<span></div>'
		+ '<div id="inventoryScreen" onClick="createInventoryHtml(account.inventory);" class="menu-button"><span>Inventory<span></div>'
		+ '<div id="scoreboardScreen" onClick="refreshScoreBoard();" class="menu-button"><span>Scoreboard<span></div>'
		+ '<div id="optionsScreen" onclick="createOptionsHtml();" class="menu-button"><span>Options</span></div>';
		menusDiv.appendChild(mainMenuDiv);
	}
}

function cleanMenus() {
	var menusDiv = document.getElementById('menus');
	menusDiv.innerHTML = '';
}

function toggleMenus() {
	var menusDiv = document.getElementById('menus');
	if (menusDiv.style.visibility !== 'visible') {
		menusDiv.style.visibility = 'visible';
	} else {
		menusDiv.style.visibility = 'hidden';
	}
}

function hiddenMenus() {
	var menusDiv = document.getElementById('menus');
	menusDiv.style.visibility = 'hidden';
}

function showMenus() {
	var menusDiv = document.getElementById('menus');
	menusDiv.style.visibility = 'visible';
}

function createPauseMenu() {
	var menusDiv = document.getElementById('menus');
	if (menusDiv) {
		var pauseMenuDiv = document.createElement('div');
		pauseMenuDiv.id = 'pauseMenu';
		pauseMenuDiv.innerHTML = '<div id="resumeGame" onClick="resumeGame();" class="menu-button"><span>Resume Game<span></div>'
		+ '<div id="inventoryScreen" onClick="createInventoryHtml(account.inventory);" class="menu-button"><span>Inventory<span></div>'
		+ '<div id="scoreboardScreen" onClick="refreshScoreBoard();" class="menu-button"><span>Scoreboard<span></div>'
		+ '<div id="optionsScreen" onclick="createOptionsHtml();" class="menu-button"><span>Options</span></div>';
		menusDiv.appendChild(pauseMenuDiv);
	}
}

function resumeGame() {
	cleanMenus();
	gameIsRunning = true;
}

function createInventoryHtml(inventory) {
	var menusDiv = document.getElementById('menus');
	var inventoryMenuDiv = document.createElement('div');
	menusDiv.appendChild(inventoryMenuDiv);
	inventoryMenuDiv.id = 'inventoryMenu';
	inventoryMenuDiv.innerHTML = '<div id="closeOptions" onClick="closeInventoryScreen();" class="menu-button"><span>Back<span></div>';
	var inventoryMenuContent = document.createElement('div');
	inventoryMenuDiv.appendChild(inventoryMenuContent);
	inventoryMenuContent.id = 'inventoryMenuContent';
	var equipedItens = document.createElement('div');
	inventoryMenuContent.appendChild(equipedItens);
	equipedItens.id = 'equipedItens';
	var weaponClass = 'weaponSlotEmpty';
	if (inventory.weapon) {
		weaponClass = inventory.weapon.action;
	}
	equipedItens.innerHTML = '<div id="weapon" class="' + weaponClass + '" onClick="createWeaponInfoHtml(account.inventory.weapon)">WEAPON</div>';
	/*
	var weapon = document.createElement('div');
	equipedItens.appendChild(weapon);
	weapon.id = 'weapon';
	if (inventory.weapon) {
		weapon.innerHTML = inventory.weapon.action;
		weapon.className = inventory.weapon.action;
	} else {
		weapon.innerHTML = 'None';
		weapon.className = 'weaponSlotEmpty';
	}
	*/
	var inventoryDiv = document.createElement('div');
	inventoryMenuContent.appendChild(inventoryDiv);
	inventoryDiv.id = 'inventory';
	if (account.inventory.itens && account.inventory.itens.length) {
		for (var i = 0; i < inventory.itens.length; i++) {
			inventoryDiv.innerHTML += '<div class="inventorySlot ' + inventory.itens[i].action + '" onClick="createItemInfoHtml(account.inventory.itens[' + i + '])"></div>';
		}
	}
}

function closeInventoryScreen() {
	var element = document.getElementById('inventoryMenu');
	element.parentNode.removeChild(element);
}

function createItemInfoHtml(item) {
	if (!item) {
		return;
	}
	var menusDiv = document.getElementById('menus');
	var itemInfoDiv = document.createElement('div');
	menusDiv.appendChild(itemInfoDiv);
	itemInfoDiv.id = 'itemInfoMenu';
	itemInfoDiv.innerHTML = '<div id="closeOptions" onClick="closeItemInfoScreen();" class="menu-button"><span>Close<span></div>'
	+ '<div class="itemInfoIcon ' + item.action + '"></div>'
	+ '<div class="itemInfoStatus">'
		+ '<div>Name: ' + item.name + '</div>'
		+ '<div>Description: ' + item.description + '</div>'
		+ '<div>Attack: ' + item.damageFactor + '</div>'
		+ '<div class="menu-button" onClick="chancePlayerWeapon(account.inventory.itens[' + account.inventory.itens.indexOf(item) + ']); reDrawInventoryContent(); closeItemInfoScreen();">Equip</div>'
		+ '<div class="menu-button" onClick="deletePlayerIten(account.inventory.itens[' + account.inventory.itens.indexOf(item) + ']); reDrawInventoryContent(); closeItemInfoScreen();">Delete</div>'
	+ '</div>';
}

function createWeaponInfoHtml(item) {
	if (!item) {
		return;
	}
	var menusDiv = document.getElementById('menus');
	var itemInfoDiv = document.createElement('div');
	menusDiv.appendChild(itemInfoDiv);
	itemInfoDiv.id = 'itemInfoMenu';
	itemInfoDiv.innerHTML = '<div id="closeOptions" onClick="closeItemInfoScreen();" class="menu-button"><span>Close<span></div>'
	+ '<div class="itemInfoIcon ' + item.action + '"></div>'
	+ '<div class="itemInfoStatus">'
		+ '<div>Name: ' + item.name + '</div>'
		+ '<div>Description: ' + item.description + '</div>'
		+ '<div>Attack: ' + item.damageFactor + '</div>'
		+ '<div class="menu-button" onClick="chancePlayerWeapon(); reDrawInventoryContent(); closeItemInfoScreen();">Unequip</div>'
	+ '</div>';
}

function reDrawInventoryContent() {
	var equipedItens = document.getElementById('equipedItens');
	var weaponClass = 'weaponSlotEmpty';
	if (account.inventory.weapon) {
		weaponClass = account.inventory.weapon.action;
	}
	equipedItens.innerHTML = '<div id="weapon" class="' + weaponClass + '" onClick="createWeaponInfoHtml(account.inventory.weapon)">WEAPON</div>';
	var inventoryDiv = document.getElementById('inventory');
	inventoryDiv.innerHTML = '';
	if (account.inventory.itens && account.inventory.itens.length) {
		for (var i = 0; i < account.inventory.itens.length; i++) {
			inventoryDiv.innerHTML += '<div class="inventorySlot ' + account.inventory.itens[i].action + '" onClick="createItemInfoHtml(account.inventory.itens[' + i + '])"></div>';
		}
	}

}

function closeItemInfoScreen() {
	var element = document.getElementById('itemInfoMenu');
	element.parentNode.removeChild(element);
}

