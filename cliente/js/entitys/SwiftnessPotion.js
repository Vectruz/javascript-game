function createSwiftnessPotion(posX, posY, posZ) {
	var planeMaterial = new THREE.MeshBasicMaterial( {map: swiftnessPotionTexture, transparent: true, side: THREE.DoubleSide});
	entity = new THREE.Mesh(new THREE.PlaneGeometry(1, 1),planeMaterial);
	scene.add(entity);
	entitys.push(entity);
	entity.item = true;
	entity.effectFactor = 1.5;
	entity.type = 'speed';
	entity.effectDuration = 5;
	entity.position.x = posX;
	entity.position.y = posY;
	entity.position.z = getZIndezPerEntity(entity);//posZ;
	entity.decayToGone = 30;
	entity.action = function() {
		if (this.decayToGone <= 0) {
			scene.remove(this);
			entitys.splice(entitys.indexOf(this), 1);
		} else {
			this.decayToGone -= delta;
		}
		if (player && player.position) {
			var dx = player.position.x - this.position.x;
			var dy = player.position.y - this.position.y;
			var d = distanceBetweenEntitys(this, player);
			if (d < 0.8) {
				player.speedEffect = 3.5;
				player.buffs.push({
					type: this.type,
					effectDuration: this.effectDuration,
					endEffect: function(target) {
						target.speedEffect = 2.5;
					}
				});
				scene.remove(this);
				entitys.splice(entitys.indexOf(this), 1);
			}
		}
	}
}
