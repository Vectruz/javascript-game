function addPlayer(posX, posY, posZ) {
	var planeMaterial = new THREE.MeshBasicMaterial( {map: characterTexture, transparent: true, side: THREE.DoubleSide});
	player = new THREE.Mesh(new THREE.PlaneGeometry(1, 1),planeMaterial);
	scene.add(player);
	entitys.push(player);
	player.player = true;
	player.isDamageAble = true;
	player.position.x = posX;
	player.position.y = posY;
	player.position.z = getZIndezPerEntity(player);//posZ;
	player.buffs = [];
	player.speedEffect = 2.5;
	player.hp = 10;
	player.hpMax = 10;
	updateHPHtml();
	player.attackDelay = 0;
	player.lives = 3;
	updateLivesHtml();
	player.kill = 0;
	updateKillsHtml();
	player.respawnPosition = {
		x: posX,
		y: posY,
		z: posZ
	};
	player.myMove = {};
	player.action = movePlayer;
	player.takeDamage = takeDamage;
	player.onDeath = onDeath;
	if (account && account.inventory && account.inventory.weapon) {
		player.mouseClick = eval(account.inventory.weapon.action);
	} else {
		player.mouseClick = playerMouseClick;
	}
}

function takeDamage() {
	this.hp--;
	createHitMark(this.position.x, this.position.y, this.position.z);
}

function onDeath() {
	createBloodPoolS0(this.position.x, this.position.y, this.position.z);
	this.lives--;
	if (this.lives > 0) {
		this.hp = this.hpMax;
		this.position.x = this.respawnPosition.x;
		this.position.y = this.respawnPosition.y;
		for (buff in this.buffs) {
			if (this.buffs[buff]) {
				this.buffs[buff].endEffect(this);
				this.buffs.splice(this.buffs.indexOf(this.buffs.buff), 1);
			}
		}
	}
	updateLivesHtml();
}

function movePlayer() {
	var mousedx = getCenterWidthScreen() - getMouseX();//e.x;
	var mousedy = getCenterHeightScreen() - getMouseY();//e.y;
	if (mousedx && mousedy) {
		this.rotation.z = Math.atan2(mousedx, mousedy);
	}
	if (this.hp <= 0 && this.lives > 0) {
		this.onDeath();
	} else if (this.lives <= 0) {
		sendScore(account, this);
		entitys.splice(entitys.indexOf(this), 1);
		scene.remove(this);
	}
	for (buff in this.buffs) {
		if (this.buffs[buff] && this.buffs[buff].effectDuration <= 0) {
			this.buffs[buff].endEffect(this);
			this.buffs.splice(this.buffs.indexOf(this.buffs.buff), 1);
			continue;
		}
		this.buffs[buff].effectDuration -= delta;
	}
	updateHPHtml()
	var xInDegree = camera.rotation.x * normalizeDegrees; // (180/Math.PI); // X from radius to degree
	var yInDegree = camera.rotation.y * normalizeDegrees; // (180/Math.PI); // Y from radius to degree
	var zInDegree = camera.rotation.z * normalizeDegrees; // (180/Math.PI); // Z from radius to degree
	var nextStep = {
		x: 0,
		y: 0,
		z: 0
	};
	var andMod = null;
	if (pressedKeys[87] && !pressedKeys[65] && !pressedKeys[83] && !pressedKeys[68]) {
		andMod = 0;
	} else
	if (pressedKeys[87] && !pressedKeys[65] && !pressedKeys[83] && pressedKeys[68]) {
		andMod = 45;
	} else
	if (!pressedKeys[87] && !pressedKeys[65] && !pressedKeys[83] && pressedKeys[68]) {
		andMod = 90;
	} else
	if (!pressedKeys[87] && !pressedKeys[65] && pressedKeys[83] && pressedKeys[68]) {
		andMod = 135;
	} else
	if (!pressedKeys[87] && !pressedKeys[65] && pressedKeys[83] && !pressedKeys[68]) {
		andMod = 180;
	} else
	if (!pressedKeys[87] && pressedKeys[65] && pressedKeys[83] && !pressedKeys[68]) {
		andMod = 225;
	} else
	if (!pressedKeys[87] && pressedKeys[65] && !pressedKeys[83] && !pressedKeys[68]) {
		andMod = 270;
	} else
	if (pressedKeys[87] && pressedKeys[65] && !pressedKeys[83] && !pressedKeys[68]) {
		andMod = 315;
	}
	if (andMod !== null) {
		var radiusY = degreesToRadius(andMod);
		nextStep.x = this.position.x + (this.speedEffect * Math.sin(radiusY)) * delta;
		nextStep.y = this.position.y + (this.speedEffect * Math.cos(radiusY)) * delta;
	}
	var doNextStep = true;
	for (var i = entitys.length - 1; i >= 0; --i) {
		if (entitys[i].isObject && distanceBetweenEntitys({position: nextStep}, entitys[i]) <= 0.85) {
			doNextStep = false;
		}
	}
	groundType = world[parseInt(nextStep.x + 0.5)][parseInt(nextStep.y + 0.5)];
	//console.log('Ground Type: ' + groundType);
	if (groundType < -0.5) {
		doNextStep = false;
	}
	if (doNextStep && nextStep.x) {
		this.position.x = nextStep.x;
	};
	if (doNextStep && nextStep.z) {
		this.position.z = nextStep.z;
	};
	if (doNextStep && nextStep.y) {
		this.position.y = nextStep.y;
	};
	if (this.attackDelay > 0) {
		this.attackDelay -= delta;
	} else if (this.attackDelay < 0) {
		this.attackDelay = 0;
	}
}

function playerMouseClick() {
	if (this.attackDelay <= 0) {
		this.attackDelay = 0.75;
		var planeMaterial = new THREE.MeshBasicMaterial( {map: stoneItemTexture, transparent: true, side: THREE.DoubleSide});
		var tempAttack = new THREE.Mesh(new THREE.PlaneGeometry(0.2, 0.2),planeMaterial);
		scene.add(tempAttack);
		entitys.push(tempAttack);
		tempAttack.speedEffect = 4;
		tempAttack.attacker = this;
		tempAttack.position.x = this.position.x;
		tempAttack.position.y = this.position.y;
		tempAttack.position.z = 0.2;
		//projector.projectVector(vector.setFromMatrixPosition(player.matrixWorld), camera);
		//vector.x = (vector.x * WIDTH_HALF) + WIDTH_HALF;
		//vector.y = - (vector.y * HEIGHT_HALF) + HEIGHT_HALF;
		//console.log(vector);
		tempAttack.hitted = [];
		tempAttack.run = 300;
		var dx = getCenterWidthScreen() - getMouseX();//mouseX;//e.x;
		var dy = getCenterHeightScreen() - getMouseY();//mouseY;//e.y;
		tempAttack.ar = Math.atan2(dx, dy);
		//console.log(entitys);
		tempAttack.action = function() {
			//console.log(this.position);
			if (this.run <= 0) {
				scene.remove(this);
				entitys.splice(entitys.indexOf(this), 1);
			} else {
				this.run--;
			}
			this.position.x += - (this.speedEffect * Math.sin(this.ar)) * delta;
			this.position.y += + (this.speedEffect * Math.cos(this.ar)) * delta;
			for (var i = entitys.length-1; 0 <= i; i--) {
				if (!entitys[i].mob) {
					continue;
				}
				var dx = this.position.x - entitys[i].position.x;
				var dy = this.position.y - entitys[i].position.y;
				var d = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
				if (d < 0.5) {
					entitys[i].hp--;
					//console.log('Vida do mob: ' + entitys[i].hp);
					if (entitys[i].hp <= 0) {
						this.attacker.kill++;
						updateKillsHtml();
					}
					scene.remove(this);
					entitys.splice(entitys.indexOf(this), 1);
					break;
				}
			}
		}
	}
}

var chancePlayerWeapon = function(item) {
	//if (!account || !account.inventory || !account.inventory.itens || !account.inventory.itens.length) {
	if (!account || !account.inventory) {
		return;
	}
	if (account.inventory.weapon) {
		if (!account.inventory.itens) {
			account.inventory.itens = [];
		}
		account.inventory.itens.unshift(account.inventory.weapon);
	}
	account.inventory.weapon = item;
	if (item) {
		account.inventory.itens.splice(account.inventory.itens.indexOf(item), 1);
	}
	if (account.inventory.weapon && player) {
		player.mouseClick = eval(account.inventory.weapon.action);
	} else if(player) {
		player.mouseClick = null;
	}
	updateInventory(account.inventory);
}

var deletePlayerIten = function(item) {
	if (!account || !account.inventory) {
		return;
	}
	account.inventory.itens.splice(account.inventory.itens.indexOf(item), 1);
	updateInventory(account.inventory);
}
