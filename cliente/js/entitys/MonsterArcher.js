function addMonsterArcher(posX, posY, posZ) {
	var planeMaterial = new THREE.MeshBasicMaterial( {map: zombieArcherTexture, transparent: true, side: THREE.DoubleSide});
	entity = new THREE.Mesh(new THREE.PlaneGeometry(1, 1),planeMaterial);
	scene.add(entity);
	entitys.push(entity);
	entity.mob = true;
	entity.speedEffect = 1.5;
	entity.isDamageAble = true;
	maxHp = Math.round(wave * 1.5);
	minHp = Math.round(wave * 0.75);
	if (maxHp < 6) {
		maxHp = 3;
	}
	if (minHp < 3) {
		minHp = 2;
	}
	entity.hp = getRandomInterval(minHp, maxHp);
	entity.position.x = posX;
	entity.position.y = posY;
	entity.position.z = getZIndezPerEntity(entity);//posZ;
	entity.drops = [
		{
			dropRate: 5,
			dropFunction: createHeart
		},
		{
			dropRate: 20,
			dropFunction: createLifePotion
		},
		{
			dropRate: 20,
			dropFunction: createStoneItem
		}
	];
	entity.attackDelay = 0;
	entity.action = function() {
		if (this.hp <= 0) {
			for (drop in this.drops) {
				if(getRandomInterval(0, 100) <= this.drops[drop].dropRate || !this.drops[drop].dropRate) {
					this.drops[drop].dropFunction(this.position.x, this.position.y, this.position.z);
				}
			}
			createBloodPoolS0(this.position.x, this.position.y, this.position.z);
			scene.remove(this);
			entitys.splice(entitys.indexOf(this), 1);
		}
		var nextStep = {};
		var dx = player.position.x - this.position.x; // dc is distance X between player and mob
		var dy = player.position.y - this.position.y; // dy is distance Y between player and mob
		this.ar = Math.atan2(dx, dy); // ar is angles in radius where the mob are loking for
		this.rotation.z = - this.ar;
		nextStep.x = this.position.x + (this.speedEffect * Math.sin(this.ar)) * delta;
		nextStep.y = this.position.y + (this.speedEffect * Math.cos(this.ar)) * delta;
		var stepBack = false;
		var stepForward = false;
		var attack = false;
		for (i in entitys) {
			if (this === entitys[i]) {
				continue;
			}
			if ((entitys[i].mob || entitys[i].player)) {
				var dWithEntity = distanceBetweenEntitys({position: {x: nextStep.x, y: nextStep.y}}, entitys[i]);
				if (dWithEntity <= 1 || (entitys[i].player && dWithEntity <= 2.4)) {
					stepBack = true;
					stepForward = false;
					break;
				} else if (!stepBack && dWithEntity >= 2.8) {
					stepForward = true;
					stepBack = false;
				} else if (entitys[i].player) {
					stepBack = false;
					stepForward = false;
					break;
				}
			}
		}
		if (distanceBetweenEntitys(this, player) <= 3) {
			attack = true;
		}
		if (stepBack) {
			this.position.x += - (this.speedEffect * Math.sin(this.ar)) * delta;
			this.position.y += - (this.speedEffect * Math.cos(this.ar)) * delta;
		}
		if (stepForward) {
			this.position.x += + (this.speedEffect * Math.sin(this.ar)) * delta;
			this.position.y += + (this.speedEffect * Math.cos(this.ar)) * delta;
		}
		if (attack && this.attackDelay <= 0) {
			this.attackDelay = 300;
			var planeMaterial = new THREE.MeshBasicMaterial( {map: arrowItemTexture, transparent: true, side: THREE.DoubleSide});
			var tempAttack = new THREE.Mesh(new THREE.PlaneGeometry(0.5, 0.5),planeMaterial);
			scene.add(tempAttack);
			entitys.push(tempAttack);
			tempAttack.attacker = this;
			tempAttack.speedEffect = 3.5;
			tempAttack.position.x = this.position.x;
			tempAttack.position.y = this.position.y;
			tempAttack.position.z = this.position.z;
			tempAttack.hitted = [];
			tempAttack.run = 200;
			tempAttack.ar = this.ar;
			tempAttack.rotation.z = - this.ar;
			tempAttack.action = function() {
				if (this.run <= 0) {
					scene.remove(this);
					entitys.splice(entitys.indexOf(this), 1);
				} else {
					this.run--;
				}
				this.position.set(
					this.position.x + (this.speedEffect * Math.sin(this.ar)) * delta,
					this.position.y + (this.speedEffect * Math.cos(this.ar)) * delta,
					0.2
				);
				var dx = player.position.x - this.position.x;
				var dy = player.position.y - this.position.y;
				var d = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
				//console.log(d);
				if (d < 0.75) {
					var hitIt = true;
					for (var i in this.hitted) {
						hitIt = false;
					}
					if (hitIt) {
						this.hitted.push(player);
						player.takeDamage();
					}
				}
			}
		} else {
			this.attackDelay--;
		}
	};
}
