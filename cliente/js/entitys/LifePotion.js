function createLifePotion(posX, posY, posZ) {
	var planeMaterial = new THREE.MeshBasicMaterial( {map: lifePotionTexture, transparent: true, side: THREE.DoubleSide});
	entity = new THREE.Mesh(new THREE.PlaneGeometry(1, 1),planeMaterial);
	scene.add(entity);
	entitys.push(entity);
	entity.item = true;
	entity.cureFactor = 5;
	entity.position.x = posX;
	entity.position.y = posY;
	entity.position.z = getZIndezPerEntity(entity);//posZ;
	entity.decayToGone = 30;
	entity.action = function() {
		if (this.decayToGone <= 0) {
			scene.remove(this);
			entitys.splice(entitys.indexOf(this), 1);
		} else {
			this.decayToGone -= delta;
		}
		if (player && player.position) {
			var dx = player.position.x - this.position.x;
			var dy = player.position.y - this.position.y;
			var d = distanceBetweenEntitys(this, player);
			if (d < 0.8) {
				player.hp += this.cureFactor;
				if (player.hp > player.hpMax) {
					player.hp = player.hpMax;
				}
				scene.remove(this);
				entitys.splice(entitys.indexOf(this), 1);
			}
		}
	}
}
