function createMonsterWolf(posX, posY, posZ) {
	var planeMaterial = new THREE.MeshBasicMaterial( {map: wolfTexture, transparent: true, side: THREE.DoubleSide});
	entity = new THREE.Mesh(new THREE.PlaneGeometry(1, 1),planeMaterial);
	scene.add(entity);
	entitys.push(entity);
	entity.mob = true;
	entity.speedEffect = 3;
	entity.isDamageAble = true;
	maxHp = Math.round(wave * 1.5);
	minHp = Math.round(wave * 1);
	if (maxHp < 6) {
		maxHp = 3;
	}
	if (minHp < 4) {
		minHp = 2;
	}
	entity.hp = getRandomInterval(minHp, maxHp);
	entity.position.x = posX;
	entity.position.y = posY;
	entity.position.z = getZIndezPerEntity(entity);//posZ;
	entity.drops = [
		{
			dropRate: 1,
			dropFunction: createHeart
		},
		{
			dropRate: 10,
			dropFunction: createLifePotion
		},
		{
			dropRate: 20,
			dropFunction: createSwiftnessPotion
		}
	];
	entity.attackDelay = 0;
	entity.action = function() {
		if (this.hp <= 0) {
			for (drop in this.drops) {
				if(getRandomInterval(0, 100) <= this.drops[drop].dropRate || !this.drops[drop].dropRate) {
					this.drops[drop].dropFunction(this.position.x, this.position.y, this.position.z);
				}
			}
			createBloodPoolS0(this.position.x, this.position.y, this.position.z);
			scene.remove(this);
			entitys.splice(entitys.indexOf(this), 1);
		}
		var nextStep = {};
		var dx = player.position.x - this.position.x; // dc is distance X between player and mob
		var dy = player.position.y - this.position.y; // dy is distance Y between player and mob
		this.ar = Math.atan2(dx, dy); // ar is angles in radius where the mob are loking for
		this.rotation.z = - this.ar;
		nextStep.x = this.position.x + (this.speedEffect * Math.sin(this.ar)) * delta;
		nextStep.y = this.position.y + (this.speedEffect * Math.cos(this.ar)) * delta;
		var stepBack = false;
		var stepForward = false;
		var attack = false;
		for (i in entitys) {
			if (this === entitys[i]) {
				continue;
			}
			if ((entitys[i].mob || entitys[i].player)) {
				var dWithEntity = distanceBetweenEntitys({position: {x: nextStep.x, y: nextStep.y}}, entitys[i]);
				if (dWithEntity <= 1) {
					stepBack = true;
					stepForward = false;
					break;
				} else if (!stepBack && dWithEntity >= 1.2) {
					stepForward = true;
					stepBack = false;
				} else if (entitys[i].player) {
					stepBack = false;
					stepForward = false;
					break;
				}
			}
		}
		if (distanceBetweenEntitys(this, player) <= 1.4) {
			attack = true;
		}
		if (stepBack) {
			console.log('Moveu para traz');
			this.position.x += - (this.speedEffect * Math.sin(this.ar)) * delta;
			this.position.y += - (this.speedEffect * Math.cos(this.ar)) * delta;
		}
		if (stepForward) {
			console.log('Moveu para frente');
			this.position.x += + (this.speedEffect * Math.sin(this.ar)) * delta;
			this.position.y += + (this.speedEffect * Math.cos(this.ar)) * delta;
		}
		if (attack && this.attackDelay <= 0) {
			//var radius   = 0.25,
    		//segments = 32,
    		//material = new THREE.LineBasicMaterial( { color: 0x0000ff } ),
    		//geometry = new THREE.CircleGeometry( radius, segments );
			this.attackDelay = 75;
			var planeMaterial = new THREE.MeshBasicMaterial( {map: wolfAttackTexture, transparent: true, side: THREE.DoubleSide});
			var tempAttack = new THREE.Mesh(new THREE.PlaneGeometry(0.5, 0.5),planeMaterial);
			//tempAttack.add( new THREE.Line( geometry, material ) );
			scene.add(tempAttack);
			entitys.push(tempAttack);
			tempAttack.attacker = this;
			tempAttack.position = this.position;
			tempAttack.hitted = [];
			tempAttack.run = 25;
			tempAttack.action = function() {
				if (this.run <= 0) {
					scene.remove(this);
					entitys.splice(entitys.indexOf(this), 1);
				} else {
					this.run--;
				}
				this.position.set(this.attacker.position.x + (0.75 * Math.sin(this.attacker.ar)), this.attacker.position.y + (0.75 * Math.cos(this.attacker.ar)), 0.2);
				//var dx = this.position.x - player.position.x;
				//var dy = this.position.y - player.position.y;
				//var d = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
				this.rotation.z = this.attacker.rotation.z;
				if (distanceBetweenEntitys(this, player) < 0.75) {
					var hitIt = true;
					for (var i in this.hitted) {
						hitIt = false;
					}
					if (hitIt) {
						this.hitted.push(player);
						player.takeDamage();
					}
				}
			}
		} else {
			if (this.attackDelay > 0) {
				this.attackDelay--;
			}
		}
	};
}
