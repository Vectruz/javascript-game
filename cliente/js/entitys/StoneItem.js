function createStoneItem(posX, posY, posZ) {
	var planeMaterial = new THREE.MeshBasicMaterial( {map: stoneItemTexture, transparent: true, side: THREE.DoubleSide});
	entity = new THREE.Mesh(new THREE.PlaneGeometry(1, 1),planeMaterial);
	scene.add(entity);
	entitys.push(entity);
	entity.item = true;
	entity.damageFactor = 1;
	entity.position.x = posX;
	entity.position.y = posY;
	entity.position.z = getZIndezPerEntity(entity);//posZ;
	entity.decayToGone = 30;
	entity.action = function() {
		if (this.decayToGone <= 0) {
			scene.remove(this);
			entitys.splice(entitys.indexOf(this), 1);
		} else {
			this.decayToGone -= delta;
		}
		if (player && player.position) {
			var dx = player.position.x - this.position.x;
			var dy = player.position.y - this.position.y;
			var d = distanceBetweenEntitys(this, player);
			if (d < 0.8) {
				minDmg = Math.round(wave/8);
				if (minDmg < 1) {
					minDmg = 1;
				}
				maxDmg = Math.round(wave/4);
				if (maxDmg < 1) {
					maxDmg = 1;
				}
				var itemDmg = getRandomInterval(minDmg, maxDmg);
				var newItem = {
					action: 'stoneItemAction',
					damageFactor: itemDmg,
					name: 'Small Stone',
					description: 'Small stone perfectly to throw in enemys'
				};
				if (account.inventory && account.inventory.itens) {
					account.inventory.itens.unshift(newItem);
				} else if (account.inventory && !account.inventory.itens) {
					account.inventory.itens = [newItem];
				}
				//chancePlayerWeapon();
				//player.mouseClick = stoneItemAction;
				updateInventory(account.inventory);
				scene.remove(this);
				entitys.splice(entitys.indexOf(this), 1);
			}
		}
	}
}

var stoneItemAction = function() {
	if (this.attackDelay <= 0) {
		this.attackDelay = 0.75;
		var planeMaterial = new THREE.MeshBasicMaterial( {map: stoneItemTexture, transparent: true, side: THREE.DoubleSide});
		var tempAttack = new THREE.Mesh(new THREE.PlaneGeometry(0.2, 0.2),planeMaterial);
		scene.add(tempAttack);
		entitys.push(tempAttack);
		tempAttack.speedEffect = 4;
		tempAttack.attacker = this;
		tempAttack.damageFactor = account.inventory.weapon.damageFactor;
		tempAttack.position.x = this.position.x;
		tempAttack.position.y = this.position.y;
		tempAttack.position.z = 0.2;
		//projector.projectVector(vector.setFromMatrixPosition(player.matrixWorld), camera);
		//vector.x = (vector.x * WIDTH_HALF) + WIDTH_HALF;
		//vector.y = - (vector.y * HEIGHT_HALF) + HEIGHT_HALF;
		//console.log(vector);
		tempAttack.hitted = [];
		tempAttack.run = 2;
		var dx = getCenterWidthScreen() - getMouseX();//mouseX;//e.x;
		var dy = getCenterHeightScreen() - getMouseY();//mouseY;//e.y;
		tempAttack.ar = Math.atan2(dx, dy);
		//console.log(entitys);
		tempAttack.action = stoneItemHitAction;
	}
}

var stoneItemHitAction = function() {
	//console.log(this.position);
	if (this.run <= 0) {
		scene.remove(this);
		entitys.splice(entitys.indexOf(this), 1);
	} else {
		this.run -= delta;
	}
	this.position.x += - (this.speedEffect * Math.sin(this.ar)) * delta;
	this.position.y += + (this.speedEffect * Math.cos(this.ar)) * delta;
	for (var i = entitys.length-1; 0 <= i; i--) {
		if (!entitys[i].mob) {
			continue;
		}
		var dx = this.position.x - entitys[i].position.x;
		var dy = this.position.y - entitys[i].position.y;
		var d = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
		if (d < 0.5) {
			entitys[i].hp -= this.damageFactor;
			createHitMark(entitys[i].position.x, entitys[i].position.y, entitys[i].position.z);
			//console.log('Vida do mob: ' + entitys[i].hp);
			if (entitys[i].hp <= 0) {
				this.attacker.kill++;
				updateKillsHtml();
			}
			scene.remove(this);
			entitys.splice(entitys.indexOf(this), 1);
			break;
		}
	}
}
