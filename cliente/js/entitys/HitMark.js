function createHitMark(posX, posY, posZ) {
	var planeMaterial = new THREE.MeshBasicMaterial( {map: hitMarkTexture, transparent: true, side: THREE.DoubleSide});
	entity = new THREE.Mesh(new THREE.PlaneGeometry(1, 1),planeMaterial);
	scene.add(entity);
	entitys.push(entity);
	entity.airEffect = true;
	entity.position.x = posX;
	entity.position.y = posY;
	entity.position.z = getZIndezPerEntity(entity);//posZ;
	entity.decayToGone = 1;
	entity.action = function() {
		if (this.decayToGone <= 0) {
			scene.remove(this);
			entitys.splice(entitys.indexOf(this), 1);
		} else {
			this.decayToGone -= delta;
		}
	}
}
