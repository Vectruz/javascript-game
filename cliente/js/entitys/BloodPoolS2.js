function createBloodPoolS2(posX, posY, posZ) {
	var planeMaterial = new THREE.MeshBasicMaterial( {map: bloodPoolS2Texture, transparent: true, side: THREE.DoubleSide});
	entity = new THREE.Mesh(new THREE.PlaneGeometry(1, 1),planeMaterial);
	scene.add(entity);
	entitys.push(entity);
	entity.groundEffect = true;
	entity.position.x = posX;
	entity.position.y = posY;
	entity.position.z = getZIndezPerEntity(entity);//posZ;
	entity.decayToGone = 6;//20;
	entity.action = function() {
		if (this.decayToGone <= 0) {
			createBloodPoolS3(this.position.x, this.position.y, this.position.z);
			scene.remove(this);
			entitys.splice(entitys.indexOf(this), 1);
		} else {
			this.decayToGone -= delta;
		}
	}
}
