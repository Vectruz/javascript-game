function createThornRose(posX, posY, posZ) {
	var planeMaterial = new THREE.MeshBasicMaterial( {map: thornRoseTexture, transparent: true, side: THREE.DoubleSide});
	entity = new THREE.Mesh(new THREE.PlaneGeometry(1, 1),planeMaterial);
	scene.add(entity);
	entitys.push(entity);
	entity.isObject = true;
	entity.damageFactor = 2;
	entity.hitted = [];
	entity.position.x = posX;
	entity.position.y = posY;
	entity.position.z = getZIndezPerEntity(entity);//posZ;
	entity.rotation.z = getRandomInterval(0, 359);
	entity.damageRound = 2;
	entity.action = function() {
		for (var i = this.hitted.length - 1; i >= 0; --i) {
			this.hitted[i].nextHitIn -= delta;
			if (this.hitted[i].nextHitIn <= 0) {
				this.hitted.splice(i, 1);
			}
		}
		for (var i = entitys.length - 1; i >= 0; --i) {
			if (distanceBetweenEntitys(this, entitys[i]) < 0.9) {
				toHit = true;
				for (var j = this.hitted.length - 1; j >= 0; --j) {
					if (entitys[i] === this.hitted[j].entity) {
						toHit = false;
					}
				}
				if (toHit) {
					this.hitted.push({entity: entitys[i], nextHitIn: this.damageRound});
					entitys[i].hp -= this.damageFactor;
				}
			}
		}
	}
}