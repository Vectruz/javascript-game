function createIronSword(posX, posY, posZ) {
	var planeMaterial = new THREE.MeshBasicMaterial( {map: ironSwordTexture, transparent: true, side: THREE.DoubleSide});
	entity = new THREE.Mesh(new THREE.PlaneGeometry(1, 1),planeMaterial);
	scene.add(entity);
	entitys.push(entity);
	entity.item = true;
	entity.damageFactor = 2;
	entity.position.x = posX;
	entity.position.y = posY;
	entity.position.z = getZIndezPerEntity(entity);//posZ;
	entity.decayToGone = 30;
	entity.action = function() {
		if (this.decayToGone <= 0) {
			scene.remove(this);
			entitys.splice(entitys.indexOf(this), 1);
		} else {
			this.decayToGone -= delta;
		}
		if (player && player.position) {
			var dx = player.position.x - this.position.x;
			var dy = player.position.y - this.position.y;
			var d = distanceBetweenEntitys(this, player);
			if (d < 0.8) {
				minDmg = Math.round(wave/4);
				if (minDmg < 2) {
					minDmg = 2;
				}
				maxDmg = Math.round(wave/2);
				if (maxDmg < 2) {
					maxDmg = 2;
				}
				var itemDmg = getRandomInterval(minDmg, maxDmg);
				var newItem = {
					action: 'ironSwordAction',
					damageFactor: itemDmg,
					name: 'Iron Sword',
					description: 'A sharp sword make of iron, cut all enemys in the way'
				};
				if (account.inventory && account.inventory.itens) {
					account.inventory.itens.unshift(newItem);
				} else if (account.inventory && !account.inventory.itens) {
					account.inventory.itens = [newItem];
				}
				//chancePlayerWeapon();
				//player.mouseClick = ironSwordAction;
				updateInventory(account.inventory);
				scene.remove(this);
				entitys.splice(entitys.indexOf(this), 1);
			}
		}
	}
};

var ironSwordAction = function() {
	if (this.attackDelay <= 0) {
		this.attackDelay = 1;
		var planeMaterial = new THREE.MeshBasicMaterial( {map: ironSwordTexture, transparent: true, side: THREE.DoubleSide});
		var tempAttack = new THREE.Mesh(new THREE.PlaneGeometry(1, 1),planeMaterial);
		scene.add(tempAttack);
		entitys.push(tempAttack);
		tempAttack.speedEffect = 4;
		tempAttack.attacker = this;
		tempAttack.damageFactor = account.inventory.weapon.damageFactor;
		tempAttack.position.x = this.position.x;
		tempAttack.position.y = this.position.y;
		tempAttack.position.z = 0.2;
		tempAttack.rotation.z = this.rotation.z;
		//projector.projectVector(vector.setFromMatrixPosition(player.matrixWorld), camera);
		//vector.x = (vector.x * WIDTH_HALF) + WIDTH_HALF;
		//vector.y = - (vector.y * HEIGHT_HALF) + HEIGHT_HALF;
		//console.log(vector);
		tempAttack.hitted = [];
		tempAttack.run = 0.75;
		var dx = getCenterWidthScreen() - getMouseX();//mouseX;//e.x;
		var dy = getCenterHeightScreen() - getMouseY();//mouseY;//e.y;
		tempAttack.ar = Math.atan2(dx, dy);
		//console.log(entitys);
		tempAttack.action = ironSwordHitAction;
	}
};

var ironSwordHitAction = function() {
	//console.log(this.position);
	if (this.run <= 0) {
		scene.remove(this);
		entitys.splice(entitys.indexOf(this), 1);
	} else {
		this.run = this.run - delta;
	}
	this.rotation.z = this.attacker.rotation.z;
	this.position.x = this.attacker.position.x - 0.75 * Math.sin(this.rotation.z);
	this.position.y = this.attacker.position.y + 0.75 * Math.cos(this.rotation.z);
	/*
	for (var i = entitys.length-1; 0 <= i; i--) {
		if (!entitys[i].mob) {
			continue;
		}
		var dx = this.position.x - entitys[i].position.x;
		var dy = this.position.y - entitys[i].position.y;
		var d = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
		if (d < 0.9) {
			entitys[i].hp -= this.damageFactor;
			if (entitys[i].hp <= 0) {
				this.attacker.kill++;
				updateKillsHtml();
			}
			scene.remove(this);
			entitys.splice(entitys.indexOf(this), 1);
			break;
		}
	}*/
	for (var i = this.hitted.length - 1; i >= 0; --i) {
		this.hitted[i].nextHitIn -= delta;
		if (this.hitted[i].nextHitIn <= 0) {
			this.hitted.splice(i, 1);
		}
	}
	for (var i = entitys.length - 1; i >= 0; --i) {
		if (!entitys[i].mob) {
			continue;
		}
		if (distanceBetweenEntitys(this, entitys[i]) < 0.9) {
			toHit = true;
			for (var j = this.hitted.length - 1; j >= 0; --j) {
				if (entitys[i] === this.hitted[j].entity) {
					toHit = false;
				}
			}
			if (toHit) {
				this.hitted.push({entity: entitys[i], nextHitIn: this.damageRound});
				console.log(entitys[i].hp + ' - ' + this.damageFactor);
				entitys[i].hp -= this.damageFactor;
				console.log(entitys[i].hp);
				createHitMark(entitys[i].position.x, entitys[i].position.y, entitys[i].position.z);
				if (entitys[i].hp <= 0) {
					this.attacker.kill++;
					updateKillsHtml();
				}
			}
		}
	}
};
