window.onkeydown = function(e) {
	code = e.keyCode ? e.keyCode : e.which;
	pressedKeys[code] = true;
	//console.log(pressedKeys);

	//console.log(code);
	if (code === 13) {
		if (!account || !account._id) {
			return;
		}
		if (gameIsRunning) {
			createPauseMenu();
		} else {
			cleanMenus();
		}
		gameIsRunning = !gameIsRunning;
	};
	if (code === 79) {
		console.log(player);
	};
	if (code === 188) {
		if (volume >= 0.1) {
			volume -= 0.1;
			document.getElementById("myRange").value = volume * 100;
		} else {
			volume = 0;
			document.getElementById("myRange").value = volume * 100;
		}
		for (var i in player.children) {
			if (player.children[i].type === 'Audio') {
				if (player.children[i].getVolume() > 0) {
					player.children[i].setVolume(player.children[i].getVolume() - 0.1);
				}
				if (player.children[i].getVolume() < 0) {
					player.children[i].setVolume(0);
				}
				document.getElementById("myRange").value = player.children[i].getVolume() * 100;
			}
		}
	}
	if (code === 190) {
		if (volume <= 0.9) {
			volume += 0.1;
			document.getElementById("myRange").value = volume * 100;
		} else {
			volume = 1;
			document.getElementById("myRange").value = volume * 100;
		}
		for (var i in player.children) {
			if (player.children[i].type === 'Audio') {
				if (player.children[i].getVolume() < 1) {
					player.children[i].setVolume(player.children[i].getVolume() + 0.1);
				}
				if (player.children[i].getVolume() > 1) {
					player.children[i].setVolume(1);
				}
				document.getElementById("myRange").value = player.children[i].getVolume() * 100;
			}
		}
	}
	if (code === 77) {
		for (var i in player.children) {
			if (player.children[i].type === 'Audio') {
				if (player.children[i].isPlaying) {
					player.children[i].pause();
				} else {
					player.children[i].play();
				}
			}
		}
	}
}

window.onkeyup = function(e) {
	code = e.keyCode ? e.keyCode : e.which;
	if (pressedKeys[code]) {
		pressedKeys[code] = false;
	}
}
