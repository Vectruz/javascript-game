var socket = io();

var teste = function() {
	socket.emit('teste', 'Teste');
}

var httpGetAsync = function(theUrl, callback) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
            callback(xmlHttp.responseText);
    }
    xmlHttp.open("GET", theUrl, true); // true for asynchronous
    xmlHttp.send(null);
}

var refreshScoreBoard = function() {
	httpGetAsync('scoreboard', function(response) {
		updateScoreboardHtml(JSON.parse(response));
	});
}

var updateInventory = function(inventory, callback) {
	account.inventory = inventory;
	socket.emit('updateInventory', inventory);
	if (callback) {
		callback();
	}
}

var sendScore = function(account, player, callback) {
	var score = {
		idAccount: account._id,
		name: account.name,
		kill: player.kill
	};
	socket.emit('newScore', score);
	if (callback) {
		console.log('With Callback');
		callback();
	} else {
		console.log('WithOut Callback');
	}
}

var acessAccount = function(account, callback) {
	socket.emit('acessAccount', account);
	if (callback) {
		callback();
	}
}

socket.on('high-score', function(response) {
	console.log(response);
	updateScoreboardHtml(response);
	/*
	if(document.getElementById('scoreboard')) {
		updateScoreboardHtml(response);
	} else {
		createScoreboardHtml(response);
	}
	*/
});

socket.on('acess-accepted', function(accountFromBack) {
	if (accountFromBack && accountFromBack._id) {
		removeLoginMenu(function() {
			account = accountFromBack;
			//restart();
			//init();
			createMainMenu();
			gameIsRunning = true;
		});
	}
});

socket.on('acess-denied', function(message) {
	alert(message);
});
