var mousePositionOnDown = {};
var mousePosition = {};

window.onmousedown = function(e) {
	//console.log(e.x + ' - ' + e.y);
	var mouseX = e.x || e.clientX;
	var mouseY = e.y || e.clientY;
	mousePositionOnDown.x = mouseX;
	mousePositionOnDown.y = mouseY;
	player.mouseClick();
};

window.onmouseup = function(e) {
	var mouseX = e.x || e.clientX;
	var mouseY = e.y || e.clientY;
	var d = distanceBetweenEntitys({position:{x: mousePositionOnDown.x, y: mousePositionOnDown.y}}, {position: {x: mouseX, y: mouseY}});
	console.log('X in Down: ' + mousePositionOnDown.x + ' |Y in Down: ' + mousePositionOnDown.y + ' |X in Up: ' + mouseX + ' |Y in Up: ' + mouseY + ' | Distance: ' + d);
}

window,onmousemove = function(e) {
	mousePosition.x = e.x || e.clientX;
	mousePosition.y = e.y || e.clientY;
}

function getMouseX() {
	return mousePosition.x;
}

function getMouseY() {
	return mousePosition.y;
}
