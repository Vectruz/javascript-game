var normalizeRadius = Math.PI/180;
var normalizeDegrees = 180/Math.PI;

function getRandomInterval(min, max) {
	var diff = max - min;
	var random = Math.random();
	return min + Math.round(random * diff);
}

var degreesToRadius = function(degree) {
	return degree * normalizeRadius;
}

var radiusToDegrees = function(radius) {
	return radius * normalizeDegrees;
}

var distanceBetweenEntitys = function(entity1, entity2) {
	if (entity1 && entity1.position && entity2 && entity2.position) {
		return Math.sqrt(Math.pow(entity1.position.x - entity2.position.x, 2) + Math.pow(entity1.position.y - entity2.position.y, 2));
	} else {
		return;
	}
}
