function checkMobsInScene(entitys) {
	for (var i in entitys) {
		if (entitys[i].mob) {
			return true;
		}
	}
	return false;
}

function getZIndezPerEntity(entity) {
	if (entity.airEffect) {
		return 0.12;
	} else if (entity.player) {
		return 0.11;
	} else if (entity.mob) {
		return 0.1;
	} else if (entity.item) {
		return 0.09;
	} else if (entity.groundEffect) {
		return 0.08;
	} else if (entity.isObject) {
		return 0.07;
	} else {
		return 0.01;
	}
}

function spawnMob() {
	var spawnX = getRandomInterval(-10, maxX + 10);
	var spawnY = getRandomInterval(-10, maxY + 10);
	if ((spawnX >= 0 && spawnX <= maxX) && (spawnY >= 0 && spawnY <= maxY)) {
		spawnMob();
		return;
	}
	for (var i = entitys.length - 1; i >= 0; --i) {
		if (distanceBetweenEntitys(entitys[i], {position: {x: spawnX, y: spawnY}}) < 1) {
			spawnMob();
			return;
		}
	}
	addMonster(spawnX, spawnY, 0.1);
}

function spawnMonsterArcher() {
	var spawnX = getRandomInterval(-10, maxX + 10);
	var spawnY = getRandomInterval(-10, maxY + 10);
	if ((spawnX >= 0 && spawnX <= maxX) && (spawnY >= 0 && spawnY <= maxY)) {
		spawnMonsterArcher();
		return;
	}
	for (var i = entitys.length - 1; i >= 0; --i) {
		if (distanceBetweenEntitys(entitys[i], {position: {x: spawnX, y: spawnY}}) < 1) {
			spawnMonsterArcher();
			return;
		}
	}
	addMonsterArcher(spawnX, spawnY, 0.1);
}

function spawnMonsterWolf() {
	var spawnX = getRandomInterval(-10, maxX + 10);
	var spawnY = getRandomInterval(-10, maxY + 10);
	if ((spawnX >= 0 && spawnX <= maxX) && (spawnY >= 0 && spawnY <= maxY)) {
		spawnMonsterWolf();
		return;
	}
	for (var i = entitys.length - 1; i >= 0; --i) {
		if (distanceBetweenEntitys(entitys[i], {position: {x: spawnX, y: spawnY}}) < 1) {
			spawnMonsterWolf();
			return;
		}
	}
	createMonsterWolf(spawnX, spawnY, 0.1);
}

function spawnThornRose() {
	var spawnX = getRandomInterval(0, maxX);
	var spawnY = getRandomInterval(0, maxY);
	if (distanceBetweenEntitys(player, {position: {x: spawnX, y: spawnY}}) < 1) {
		spawnThornRose();
		return;
	}
	for (var i = entitys.length - 1; i >= 0; --i) {
		if ((entitys[i].isObject || entitys[i].player) && entitys[i].position && entitys[i].position.x && entitys[i].position.x === spawnX && entitys[i].position.y && entitys[i].position.y === spawnY) {
			spawnThornRose();
			return;
		}
	}
	groundType = getGroundType(spawnX, spawnY);
	if(groundType < -0.5 || groundType > 0.5) {
		spawnThornRose();
		return;
	}
	createThornRose(spawnX, spawnY, 0.2);
}

function spawnPlayer() {
	var spawnX = getRandomInterval(0, maxX);
	var spawnY = getRandomInterval(0, maxY);
	groundType = getGroundType(spawnX, spawnY);
	if(groundType < -0.5) {
		spawnPlayer();
		return;
	}
	addPlayer(spawnX, spawnY, 0.1);
}

function getGroundType(x, y) {
	return world[x][y];
}
