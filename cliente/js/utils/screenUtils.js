var getCenterWidthScreen = function() {
	return window.innerWidth / 2;
}

var getCenterHeightScreen = function() {
	return window.innerHeight / 2;
}

var getMarginLeftGameScreen = function() {
	var canvas = document.getElementById('gameScreen');
	var canvasStyle = getComputedStyle(canvas);
	return canvasStyle.marginLeft;
}