var scene, camera, renderer;
var sceneOrtho, cameraOrtho;

var idScene;

var account;

var clock = new THREE.Clock();
var delta = 0.02;

var GAP_TOP_SCREEN = 0;
var WIDTH_SCREEN = window.innerWidth;
var HEIGHT_SCREEN = window.innerHeight;
var WIDTH_SCREEN_HALF = window.innerWidth / 2;
var HEIGHT_SCREEN_HALF = window.innerHeight / 2;
var WIDTH  = 640;//window.innerWidth;
var HEIGHT = 480;//window.innerHeight;
var WIDTH_HALF = WIDTH / 2;
var HEIGHT_HALF = HEIGHT / 2;
var cube, cube2, grounds, player;
var entitys = [];
var SPEED = 0.01;
var code;
var volume = 0.5;
var gameIsRunning = true;

var pressedKeys = [];
var fakeMouse;

var wave = 1;

var world = [];
var maxX = 20;
var maxY = 15;

//var vector = new THREE.Vector3();
//var projector = new THREE.Projector();
var audioLoader = new THREE.AudioLoader();
/*
var socket = io();
var mandarMensagem = function(){
	socket.emit('chat message', 'Enviou');
	return false;
};
socket.on('chat message', function(msg){
	console.log(msg);
});
mandarMensagem();
*/

function restart() {
	wave = 1;
	entitys = [];
	for (var i in player.children) {
		if (player.children[i].type === 'Audio') {
			player.children[i].stop();
		}
	}
	player = null;
	document.getElementById('canvas').parentNode.removeChild(document.getElementById('canvas'));
	cancelAnimationFrame(idScene);
	init();
}

function init() {
	scene = new THREE.Scene();
	initCamera();
	initRenderer();

	var element = document.getElementById('gameScreen');
	renderer.domElement.id = 'canvas';
	element.appendChild(renderer.domElement);
	// ... (scene initialization)
	initSurvivalMode();
	initSound();
	// ...
	render();
	cleanMenus();
	gameIsRunning = true;
}

function login() {
	account = getMenuLoginValues();
	acessAccount(account);
}

function goToScoreBoard() {
	window.location.replace('http://' + window.location.host + '/page/scoreboard');
}

function initSound() {
	var audioListener = new THREE.AudioListener();
	camera.add(audioListener);

	var backgroundSound = new THREE.PositionalAudio(audioListener);
	audioLoader.load('../sounds/Kalimba.mp3', function(buffer) {
		backgroundSound.setBuffer(buffer);
		backgroundSound.setRefDistance(20);
		backgroundSound.setVolume(volume);
		backgroundSound.play();
	});
	player.add(backgroundSound);
}

function addPlane(textura, posX, posY, posZ, hexColor) {
	var planeMaterial = new THREE.MeshBasicMaterial( {map: textura,side: THREE.DoubleSide});
	var plane = new THREE.Mesh(new THREE.PlaneGeometry(1, 1),planeMaterial);
	if (posX) {
		plane.position.x = posX;
	}
	if (posY) {
		plane.position.y = posY;
	}
	if (posZ) {
		plane.position.z = posZ;
	}
	//plane.position.set( 1, 2, 0 );
	scene.add(plane);
}

function initCamera() {
	camera = new THREE.PerspectiveCamera(45, WIDTH / HEIGHT, 0.1, 50);
	camera.position.set(0, 0, 5);
	camera.rotation.order = 'YXZ';
	camera.lookAt(scene.position);
}

function initRenderer() {
	renderer = new THREE.WebGLRenderer({ antialias: true });
	renderer.setSize(WIDTH, HEIGHT);
}

function addCubeWithColor(posX, posY, posZ, hexColor) {
	if (!hexColor) {
		hexColor = 0xff00ff;
	};
	cube = new THREE.Mesh(new THREE.CubeGeometry(1, 1, 1), new THREE.MeshBasicMaterial({color: hexColor}));
	scene.add(cube);
	cube.position.x = posX;
	cube.position.y = posY;
	cube.position.z = posZ;
	cube.cube = true;
}

function initSurvivalMode() {
	maxX = getRandomInterval(15, 25);
	maxY = getRandomInterval(15, 25);
	var tempTexture;
	var timestamp = Date.now();
	var seed = timestamp; // changing this to random this code
	world = [];
	for (var x = 0 - 5; x <= maxX + 5; x++) {
		world[x] = [];
		for (var y = 0 - 5; y <= maxY + 5; y++) {
			var nx = x / maxX - 0.5 * seed;
			var ny = y / maxY - 0.5 * seed;
			world[x][y] = noise.simplex3(nx, ny, 0);
			if (x <= 0 || x >= maxX || y <= 0 || y >= maxY) {
				tempTexture = waterTexture;
				world[x][y] = -1;
			} else if(world[x][y] < -0.5) {
				tempTexture = waterTexture;
			} else if (world[x][y] > 0.5) {
				tempTexture = stoneTexture;
			} else {
				tempTexture = grassTexture;
			}
			addPlane(tempTexture, x, y, 0);
		}
	}
	/*
	for (var x = 0; x < 20; x++) {
		for (var y = 0; y < 15; y++) {
			if (x === 0 || x === 19 || y === 0 || y === 14) {
				tempTexture = stoneTexture;
			} else {
				tempTexture = grassTexture;
			}
			addPlane(tempTexture, x, y, 0);
		}
	}
	*/
	spawnPlayer();
	var countThornRose = 0;
	while(countThornRose < 5) {
		spawnThornRose();
		countThornRose++;
	}
	//addPlayer(10, 7, 0.1);
	//addMonster(0, 0, 0.1);
}

function moveCamTarget(target) {
	camera.position.x = target.position.x;
	camera.position.y = target.position.y;
	camera.position.z = target.position.z + 10;
}

function render() {
	/* Bloco de codigo para simular pc com baixo nível de processamento
	var start = new Date().getTime();
	for (var i = 0; i < 1e7; i++) {
		if ((new Date().getTime() - start) > 100){
			break;
		}
	}
	*/
	delta = clock.getDelta();
	//console.log(delta);
	if (gameIsRunning) {
		if(!checkMobsInScene(entitys)) {
			wave++;
			for (var i = 0; i < wave; i++) {
				if (i !== 0 && i % 3 === 0) {
					spawnMonsterArcher();
				}
				if (i !== 0 && i % 5 === 0) {
					spawnMonsterWolf();
				}
				spawnMob();
			}
		}
		for (var i = scene.children.length - 1; i >= 0; i--) {
			if (scene.children[i].action) {
				scene.children[i].action();
			}
		}
	};
	idScene = requestAnimationFrame(render);
	moveCamTarget(player);
	renderer.render(scene, camera);
}

createLoginMenu();
