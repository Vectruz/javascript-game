var express = require('express');
var path = require('path');
var app = express();

var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://localhost:27017/game-teste';

var conexao = null;
var localUrl = 'http://localhost:3000';

var insertObjScore = function(obj, callback) {
	conexao.collection('score').insertOne(obj, function(err, result) {
		assert.equal(err, null);
		console.log("Inserted something.");
		if (callback) {
			callback();
		}
	});
};

var insertObjAccount = function(obj, callback) {
	conexao.collection('account').insertOne(obj, function(err, result) {
		assert.equal(err, null);
		console.log("Inserted account.");
		if (callback) {
			callback();
		}
	});
};

var insertObjInventory = function(obj, callback) {
	if (obj && !obj.idAccount) {
		console.log('Inventory need id account');
		return;
	}
	conexao.collection('inventory').insertOne(obj, function(err, result) {
		assert.equal(err, null);
		console.log("Inserted inventory.");
		if (callback) {
			callback();
		}
	});
};

var updateObjInventory = function(obj, callback) {
	conexao.collection('inventory').update({_id: obj._id}, obj, { upsert: true}, function(err, objs) {
		assert.equal(err, null);
		console.log("Updated inventory.");
		if (callback) {
			callback();
		}
	});
};

var findObjInventory = function(inventory, callback) {
	conexao.collection('inventory').find({_id: inventory._id}).toArray(function(err, objs){
		if (err) {
			callback(err);
		} else {
			callback(null, objs);
		}
	});
};

var findObjInventoryByAccount = function(account, callback) {
	console.log('Find inventory of account ' + account.name);
	conexao.collection('inventory').find({idAccount: account._id}).toArray(function(err, objs){
		if (err) {
			callback(err);
		} else {
			callback(null, objs);
		}
	});
};

var acessAccount = function(account, callback) {
	console.log('Find Account ' + account.name + ' and check password');
	conexao.collection('account').find({name: account.name, password: account.password}).toArray(function(err, objs){
		if (err) {
			callback(err);
		} else {
			callback(null, objs);
		}
	});
};

var findObjsScore = function(callback) {
	conexao.collection('score').find().sort({kill: -1}).limit(10).toArray(function(err, objs){
		if (err) {
			callback(err);
		} else {
			callback(null, objs);
		}
	});
};

var findObjAccount = function(account, callback) {
	console.log('Find Account ' + account.name);
	conexao.collection('account').find({name: account.name}).toArray(function(err, objs){
		if (err) {
			callback(err);
		} else {
			callback(null, objs);
		}
	});
};

var createInventory = function(account, callback) {
	var inventory = {
		idAccount: account._id,
		weapon: {
			name: 'Small Stone',
			description: 'Small stone perfectly to throw in enemys',
			action: 'stoneItemAction',
			damageFactor: 1
		}
	}
	insertObjInventory(inventory, function() {
		findObjInventoryByAccount(account, callback);
	});
};

var loginAccount = function(socket, account) {
	console.log('User: ' + account.name + ' |Password: ' + account.password);
	findObjAccount(account, function(err, objs) {
		console.log(objs);
		if (objs && objs.length) {
			console.log('Account already Exists... verify password');
			acessAccount(account, function(err, accounts) {
				if (accounts && accounts.length) {
					console.log('Acess accepted!');
					findObjInventoryByAccount(accounts[0], function (err, inventorys) {
						console.log('After acess accepted');
						accounts[0].inventory = inventorys[0];
						console.log(accounts);
						socket.emit('acess-accepted', accounts[0]);
					});
				} else {
					console.log('Acess denied!');
					socket.emit('acess-denied', 'Acess denied! Password or account does not match!');
				}
			});
		} else {
			console.log('New Account');
			insertObjAccount(account, function() {
				findObjAccount(account, function(err, accounts) {
					if (accounts && accounts.length) {
						console.log('Account created!');
						createInventory(accounts[0], function(err, inventorys) {
							console.log('After create inventory');
							accounts[0].inventory = inventorys[0];
							console.log(accounts);
							socket.emit('acess-accepted', accounts[0]);
						});
					} else {
						console.log('Error to create account!');
						socket.emit('acess-denied', 'Error to create account!');
					}
				});
			});
		}
	});
};

MongoClient.connect(url, function(err, db) {
	assert.equal(null, err);
	conexao = db;
	// Testing inventory functions
	/*
	findObjAccount({name: 'a', password: 'a'}, function(err, objs) {
		console.log(objs);
		findObjInventoryByAccount(objs[0], function(err, objs) {
			console.log(objs);
			if (objs && !objs[0]) {
				findObjAccount({name: 'a', password: 'a'}, function(err, objs) {
					insertObjInventory({idAccount: objs[0]._id, teste: 'teste'});
				});
			} else {
				objs[0].edited = true;
				updateObjInventory(objs[0]);
			}
		});
	});
	*/
});

app.set('port', 3000);

app.use(express.static(path.join(__dirname, 'cliente')));

app.get('/scoreboard', function (req, res) {
	findObjsScore(function(err, objs) {
		//socket.emit('high-score', objs);
		res.status(200).json(objs);
	});
	//res.send('Hello World!');
});

app.get('/page/scoreboard', function (req, res) {
	findObjsScore(function(err, objs) {
		html = '<script>var goToHome = function(){window.location.replace("http://" + window.location.host);}</script>' +
		'<div>' +
			'<div style="text-align:center; padding: 10px;">ScoreBoard</div>' +
			'<div style="text-align:center; padding: 5px;"><button onClick="goToHome();">Back</button></div>' +
			'<div>' +
				'<div style="display: flex">' +
					'<div style="width: 50%; text-align: right; padding: 5px;">Name</div><div style="width: 50%; padding: 5px;">Score</div>' +
				'</div>';
			for (var i = 0; i < objs.length; i++) {
				html += '<div style="display: flex">';
					html += '<div style="width: 50%; text-align: right; padding: 5px;">' + objs[i].name + '</div>';
					html += '<div style="width: 50%; padding: 5px;">' + objs[i].kill + '</div>';
				html += '</div>';
			}
			html += '</div>';
		html += '</div>';
		res.send(html);
	});
});

var server = app.listen(app.get('port'), function() {
	var port = server.address().port;
	console.log('Magic happens on port ' + port);
});
var io = require('socket.io')(server);

io.on('connection', function(socket){
	console.log('a user connected');
	findObjsScore(function(err, objs) {
		//socket.emit('high-score', objs);
	});
	socket.on('disconnect', function(){
		console.log('user disconnected');
	});
	socket.on('teste', function(key){
		console.log('Funcionou! ' + key);
	});
	socket.on('acessAccount', function(account){
		loginAccount(socket, account);
	});
	socket.on('newScore', function(score) {
		score.idAccount = ObjectId(score.idAccount);
		insertObjScore(score);
	});
	socket.on('updateInventory', function(inventory) {
		inventory._id = ObjectId(inventory._id);
		inventory.idAccount = ObjectId(inventory.idAccount);
		updateObjInventory(inventory);
		/*
		findObjInventory(inventory, function(err, inventorys) {
			console.log(inventorys);
			console.log(inventory);
			//updateObjInventory(inventory);
		});
		*/
	});
});
